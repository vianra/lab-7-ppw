from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from django.test import LiveServerTestCase
from selenium import webdriver
import unittest
from selenium.webdriver.chrome.options import Options

# # Create your tests here.

class story7(TestCase):

    def test_page(self):
        response = Client().get("")
        self.assertEqual(response.status_code,200)
    
    def test_page_template(self):
        response = Client().get("")
        self.assertTemplateUsed(response,'index.html')
    
    def test_greetings(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Hi! I'm Rony Agus Vian",html_response)

class FuncionalTest7(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.get('http://localhost:8000')
    
    def tearDown(self):
        self.selenium.quit()

    def test_mode(self):
        browser = self.selenium
        body_background = browser.find_element_by_id('body').value_of_css_property('background')
        self.assertIn("https://mdbootstrap.com/img/Photos/Horizontal/Nature/full%20page/img(11).jpg", body_background)
        switch_button = browser.find_element_by_class_name("switch")
        switch_button.click()
        body_background = browser.find_element_by_id('body').value_of_css_property('background')
        self.assertIn("https://mdbootstrap.com/img/Photos/Horizontal/Nature/full%20page/img(20).jpg", body_background)
    
    def test_accordion_1(self):
        browser = self.selenium
        accordion = browser.find_element_by_xpath('//*[@id="ui-id-1"]')
        accordion.click()
        self.assertIn("Currently learning computer science at Faculty of Computer Science",browser.page_source)
    
    def test_accordion_2(self):
        browser = self.selenium
        accordion = browser.find_element_by_xpath('//*[@id="ui-id-3"]')
        accordion.click()
        self.assertIn("Junior member of Ristek Competitive Programming SIG",browser.page_source)
    
    def test_accordion_3(self):
        browser = self.selenium
        accordion = browser.find_element_by_xpath('//*[@id="ui-id-5"]')
        accordion.click()
        self.assertIn("Contestant of ICPC Jakarta Regional 2019",browser.page_source)

