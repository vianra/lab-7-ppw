$(document).ready(function() {

    $(function() {
        $("#accordion").accordion({
            collapsible: true,
            active: false,
        });
    });

    $("input").change(function() {
        if (this.checked) {
            $('link[href="/static/css/style.css"]').attr('href', '/static/css/style1.css');
            document.getElementById('best').innerHTML = '<strong>Best Mode</strong>';
        } else {
            $('link[href="/static/css/style1.css"]').attr('href', '/static/css/style.css');
            document.getElementById('best').innerHTML = '<strong>Great Mode</strong>';
        }
    });


});
